import {startCase, toLower} from 'lodash';

function convertToTitle(str: string) {
  return startCase(toLower(str));
}

export default convertToTitle;
