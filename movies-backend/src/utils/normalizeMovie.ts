import convertToTitle from './convertToTitle';
import {Movie} from '../models';

function normalizeMovie(movie: Movie) {
  return {
    id: movie.id,
    title: convertToTitle(movie.title),
    description: movie.description,
    year: movie.year,
  };
}

export default normalizeMovie;
