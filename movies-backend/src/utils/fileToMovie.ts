import Papa from 'papaparse';
import {Movie} from '../models';
import convertToTitle from './convertToTitle';

// This should be parametrizable
const DELIMITER = ';';

// TODO: Remove any
function fileToMovie(file: any): Movie[] {
  // TODO: This should be properly converted to string eventually
  const stringContent: string = file[0].buffer.toString();
  return parserAlgorythm(stringContent);
}

function parserAlgorythm(stringContent: string): Movie[] {
  // TODO: Should be a common interface instead of any so that we can change library without pain
  const result: any = Papa.parse(stringContent, {
    delimiter: DELIMITER,
    skipEmptyLines: true,
  });

  if (hasErrors(result)) throw Error('Failed to parse uploaded file');
  return result.data.map(
    (parsed: any[]) =>
      new Movie({
        title: convertToTitle(parsed[0]),
        description: parsed[1],
        year: Number(parsed[2]),
      }),
  );
}

function hasErrors(result: any) {
  return !!result.errors && result.errors.length > 0;
}

export default fileToMovie;
