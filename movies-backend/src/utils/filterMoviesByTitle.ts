import {uniqBy, differenceWith} from 'lodash';
import {Movie} from '../models/movie.model';
import {MovieRepository} from '../repositories';

async function filterMoviesByTitle(
  movies: Movie[],
  repository: MovieRepository,
): Promise<Movie[]> {
  const uniqueSentMovies = uniqBy(movies, 'title');
  const sentTitles = uniqueSentMovies.map(movie => movie.title);
  const savedMovies = await repository.find({
    where: {title: {inq: sentTitles}},
  });
  return differenceWith(
    uniqueSentMovies,
    savedMovies,
    (sentMovie, savedMovie) => {
      return savedMovie.title === sentMovie.title;
    },
  );
}

export default filterMoviesByTitle;
