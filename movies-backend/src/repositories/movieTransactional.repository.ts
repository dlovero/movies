import {inject} from '@loopback/core';
import {
  juggler,
  DefaultTransactionalRepository,
} from '@loopback/repository';
import {Movie} from '../models';

export class MovieTransactionalRepository extends DefaultTransactionalRepository<
  Movie,
  typeof Movie.prototype.title
> {
  constructor(@inject('datasources.mysql') ds: juggler.DataSource) {
    super(Movie, ds);
  }
}
