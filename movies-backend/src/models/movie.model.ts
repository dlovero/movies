import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    mysql: {schema: 'moviesdb', table: 'movies'},
  },
})
export class Movie extends Entity {
  // TODO: Mask this property when exposed
  @property({
    type: 'number',
    id: true,
    generated: true,
    required: false,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  title: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property({
    type: 'number',
    required: true,
  })
  year: number;

  constructor(data?: Partial<Movie>) {
    super(data);
  }
}

export interface MovieRelations {
  // describe navigational properties here
}

export type MovieWithRelations = Movie & MovieRelations;
