import {
  expect,
  createStubInstance,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import filterMoviesByTitle from '../../utils/filterMoviesByTitle';
import {MovieRepository} from '../../repositories';
import {givenAMovie} from '../acceptance/test-helper';

describe('When filtering files', () => {
  beforeEach(givenStubbedRepository);
  let repository: StubbedInstanceWithSinonAccessor<MovieRepository>;

  describe('and there is no saved files in the database', () => {
    beforeEach(() => repository.stubs.find.resolves([]));

    it('should return nothing if we do not send any to filter', async () => {
      const subject = await filterMoviesByTitle([], repository);
      expect(subject).is.empty();
    });

    it('should return all the movies if none of them are duplicated', async () => {
      const aMovie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, title: 'Die hard'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject[0]).to.containEql(aMovie);
      expect(subject[1]).to.containEql(anotherMovie);
      expect(subject).has.lengthOf(2);
    });

    it('should return the non duplicated movies if they have the same title', async () => {
      const aMovie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, description: 'Another one'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject[0]).to.containEql(aMovie);
      expect(subject).has.lengthOf(1);
    });
  });

  describe('and there are saved files in the database', () => {
    beforeEach(() =>
      repository.stubs.find.resolves([
        givenAMovie({title: 'Some other'}),
        givenAMovie({title: 'Matrix'}),
      ]),
    );

    it('should return nothing if we do not send any to filter', async () => {
      const subject = await filterMoviesByTitle([], repository);
      expect(subject).is.empty();
    });

    it('should return all the movies if none of them are duplicated by title with the ones in the db, and they are not duplicated between each other', async () => {
      const aMovie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, title: 'Die hard'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject[0]).to.containEql(aMovie);
      expect(subject[1]).to.containEql(anotherMovie);
      expect(subject).has.lengthOf(2);
    });

    it('should return the non duplicated movies if they have the same title between each others but not with the ones in the db', async () => {
      const aMovie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, title: 'Die hard'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject[0]).to.containEql(aMovie);
      expect(subject[1]).to.containEql(anotherMovie);
      expect(subject).has.lengthOf(2);
    });

    it('should return the non duplicated movies if they have the same title with some of the ones in the db', async () => {
      const aMovie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, title: 'Matrix'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject[0]).to.containEql(aMovie);
      expect(subject).has.lengthOf(1);
    });

    it('should return no movies if they all have duplicated title with some of the ones in the db', async () => {
      const aMovie = givenAMovie({title: 'Some other'});
      const anotherMovie = givenAMovie({id: 2, title: 'Matrix'});
      const subject = await filterMoviesByTitle(
        [aMovie, anotherMovie],
        repository,
      );

      expect(subject).is.empty();
    });
  });

  function givenStubbedRepository() {
    repository = createStubInstance(MovieRepository);
  }
});
