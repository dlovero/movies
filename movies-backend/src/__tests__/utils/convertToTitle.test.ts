import {expect} from '@loopback/testlab';
import convertToTitle from '../../utils/convertToTitle';

describe('When converting a title', () => {
  let title: string;

  describe('and the title is an empty string', () => {
    beforeEach(() => (title = ''));
    it('should return an empty string', () =>
      expect(convertToTitle(title)).to.equal(''));
  });

  describe('and the title is one word mayus', () => {
    beforeEach(() => (title = 'TITLE'));
    it('should return the word properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title'));
  });

  describe('and the title is one word minus', () => {
    beforeEach(() => (title = 'title'));
    it('should return the word properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title'));
  });

  describe('and the title is one word Pascal case', () => {
    beforeEach(() => (title = 'Title'));
    it('should return the word properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title'));
  });

  describe('and the title more than one word mayus', () => {
    beforeEach(() => (title = 'TITLE TITLE TITLE TITLE'));
    it('should return the words properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title Title Title Title'));
  });

  describe('and the title more than one word minus', () => {
    beforeEach(() => (title = 'title title title'));
    it('should return the words properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title Title Title'));
  });

  describe('and the title more than one word Pascal case', () => {
    beforeEach(() => (title = 'Title Title'));
    it('should return the words properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title Title'));
  });

  describe('and the title more than one word weird formatted', () => {
    beforeEach(() => (title = 'title tITle Title'));
    it('should return the words properly formatted', () =>
      expect(convertToTitle(title)).to.equal('Title Title Title'));
  });
});
