import {expect} from '@loopback/testlab';
import fileToMovie from '../../utils/fileToMovie';
import {Movie} from '../../models';

describe('When converting a csv file to movies', () => {
  let fileStub: any;

  describe('and there is only one valid row', () => {
    beforeEach(() => givenACsvFile());

    it('should return one movie', () => {
      const subject = fileToMovie(fileStub);
      expect(subject[0]).to.containEql(
        new Movie({title: 'Iron Man', description: 'Descripcion', year: 2008}),
      );
      expect(subject).has.lengthOf(1);
    });
  });

  describe('and there is one row with empty values', () => {
    beforeEach(() => givenACsvFile(';;;'));
    it('should return one movie', () => {
      const subject = fileToMovie(fileStub);
      expect(subject[0]).to.containEql(
        new Movie({title: '', description: '', year: 0}),
      );
      expect(subject).has.lengthOf(1);
    });
  });

  describe('and there is more than one valid row', () => {
    beforeEach(() =>
      givenACsvFile(`Iron man;Descripcion;2008;
      DieHard;Descripcion;2008;`),
    );

    it('should return all the parsed movies', () => {
      const subject = fileToMovie(fileStub);
      expect(subject[0]).to.containEql(
        new Movie({title: 'Iron Man', description: 'Descripcion', year: 2008}),
      );
      expect(subject[1]).to.containEql(
        new Movie({title: 'Diehard', description: 'Descripcion', year: 2008}),
      );
      expect(subject).has.lengthOf(2);
    });
  });

  describe('and there is a duplicated row', () => {
    beforeEach(() =>
      givenACsvFile(`Iron man;Descripcion;2008;
      Iron man;Descripcion;2008;`),
    );

    it('should return all the duplicated movies', () => {
      const subject = fileToMovie(fileStub);
      expect(subject[0]).to.containEql(
        new Movie({title: 'Iron Man', description: 'Descripcion', year: 2008}),
      );
      expect(subject[1]).to.containEql(
        new Movie({title: 'Iron Man', description: 'Descripcion', year: 2008}),
      );
      expect(subject).has.lengthOf(2);
    });
  });

  describe('and there is an empty line and some valid rows', () => {
    beforeEach(() =>
      givenACsvFile(`Iron man;Descripcion;2008;

    DieHard;Descripcion;2008;`),
    );

    it('should return all the parsed movies and ignore the blank line', () => {
      const subject = fileToMovie(fileStub);
      expect(subject[0]).to.containEql(
        new Movie({title: 'Iron Man', description: 'Descripcion', year: 2008}),
      );
      expect(subject[1]).to.containEql(
        new Movie({title: 'Diehard', description: 'Descripcion', year: 2008}),
      );
      expect(subject).has.lengthOf(2);
    });
  });

  describe('and there is an empty file', () => {
    beforeEach(() => givenACsvFile(''));

    it('should return an empty array', () => {
      const subject = fileToMovie(fileStub);
      expect(subject).has.lengthOf(0);
    });
  });

  function givenACsvFile(string?: string) {
    fileStub = [{buffer: string ?? 'Iron man;Descripcion;2008'}];
  }
});
