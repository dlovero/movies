import {StarterApplication} from '../..';
import {
  createRestAppClient,
  givenHttpServerConfig,
  Client,
} from '@loopback/testlab';
import {Movie} from '../../models';

export async function setupApplication(): Promise<AppWithClient> {
  const restConfig = givenHttpServerConfig({
    // Customize the server configuration here.
    // Empty values (undefined, '') will be ignored by the helper.
    //
    // host: process.env.HOST,
    // port: +process.env.PORT,
  });

  const app = new StarterApplication({
    rest: restConfig,
  });

  await app.boot();
  await app.start();

  const client = createRestAppClient(app);

  return {app, client};
}

export interface AppWithClient {
  app: StarterApplication;
  client: Client;
}

export function givenAMovie({
  id,
  title,
  description,
  year,
}: {
  id?: number;
  title?: string;
  description?: string;
  year?: number;
}) {
  return new Movie({
    id: id ?? 1,
    title: title ?? 'Iron man',
    description: description ?? 'desc',
    year: year ?? 2008,
  });
}
