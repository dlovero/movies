import {
  createStubInstance,
  expect,
  sinon,
  StubbedInstanceWithSinonAccessor,
} from '@loopback/testlab';
import {MovieRepository} from '../../repositories';
import {Movie} from '../../models';
import {MoviesController} from '../../controllers';
import {givenAMovie} from './test-helper';

/*
 * ACLARACIÓN: Me gusta dejar bien declarativas las aserciones aunque eso implique repetir código
 * Que se vea a simple vista QUE se esta testeando, y QUE espero como resultado.
 * No soy demasiado fan de los tests de controller igualmente, sobre todo en este caso donde
 * el framework te auto genera endpoints que ya cumplen con el requerimiento.
 * Prácticamente sería "testear el framework". Pero por otro lado es una manera de documentar la interfaz
 * deseada para cada endpoint de manera viva y mantenible.
 */

describe('MoviesController', () => {
  let repository: StubbedInstanceWithSinonAccessor<MovieRepository>;
  beforeEach(givenStubbedRepository);

  describe('GET /movies/count', () => {
    it('retrieves the count of total movies', async () => {
      const controller = new MoviesController(repository);
      repository.stubs.count.resolves({count: 15});

      const details = await controller.count();

      expect(details).to.containEql({count: 15});

      sinon.assert.called(repository.stubs.count);
    });
  });

  describe('GET /movies/:id', () => {
    it('retrieves details of a movie', async () => {
      const movie = givenAMovie({});
      const controller = new MoviesController(repository);
      repository.stubs.findById.resolves(movie);

      const details = await controller.findById(1);

      expect(details).to.containEql({
        id: 1,
        title: 'Iron man',
        description: 'desc',
        year: 2008,
      });

      sinon.assert.calledWith(repository.stubs.findById, 1);
    });
  });

  describe('GET /movies', () => {
    it('retrieves details of all movies', async () => {
      const movie = givenAMovie({});
      const anotherMovie = givenAMovie({id: 2, title: 'Die hard'});
      const controller = new MoviesController(repository);
      repository.stubs.find.resolves([movie, anotherMovie]);

      const details = await controller.find();

      expect(details[0]).to.containEql({
        id: 1,
        title: 'Iron man',
        description: 'desc',
        year: 2008,
      });
      expect(details[1]).to.containEql({
        id: 2,
        title: 'Die hard',
        description: 'desc',
        year: 2008,
      });
      expect(details).to.has.lengthOf(2);

      sinon.assert.called(repository.stubs.find);
    });
  });

  describe('POST /movies', () => {
    it('retrieves details of the created movie', async () => {
      const movieDetails = {
        id: 1,
        title: 'New movie',
        description: 'description',
        year: 2009,
      };
      const movie = givenAMovie(movieDetails);
      const controller = new MoviesController(repository);
      repository.stubs.create.resolves(movie);

      const details = await controller.create(new Movie(movieDetails));

      expect(details).to.containEql({
        id: 1,
        title: 'New movie',
        description: 'description',
        year: 2009,
      });

      sinon.assert.calledWith(repository.stubs.create, {
        description: 'description',
        id: 1,
        title: 'New Movie',
        year: 2009,
      });
    });
  });

  describe('PATCH /movies/:id', () => {
    it('retrieves void OK when updating a movie', async () => {
      const movieDetails = {
        id: 1,
        title: 'New movie',
        description: 'description',
        year: 2009,
      };
      const movie = givenAMovie(movieDetails);
      const controller = new MoviesController(repository);
      repository.stubs.updateById.resolves();

      const details = await controller.updateById(1, movie);

      expect(details).is.undefined();

      sinon.assert.calledWith(repository.stubs.updateById, 1, {
        description: 'description',
        id: 1,
        title: 'New Movie',
        year: 2009,
      });
    });
  });

  describe('DELETE /movies/:id', () => {
    it('retrieves details of the created movie', async () => {
      const controller = new MoviesController(repository);
      repository.stubs.deleteById.resolves();

      const details = await controller.deleteById(1);

      expect(details).is.undefined();

      sinon.assert.calledWith(repository.stubs.deleteById, 1);
    });
  });

  function givenStubbedRepository() {
    repository = createStubInstance(MovieRepository);
  }
});
