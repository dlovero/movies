import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {Movie} from '../models';
import {MovieRepository} from '../repositories';
import normalizeMovie from '../utils/normalizeMovie';

/*
 * ACLARACIÓN: Eliminé el endpoint PUT /movies/:id y PATCH /movies
 * simplemente para simplificar la interfaz expuesta en el api explorer
 */

export class MoviesController {
  constructor(
    @repository(MovieRepository)
    public movieRepository: MovieRepository,
  ) {}

  @post('/movies', {
    responses: {
      '200': {
        description: 'Movie model instance',
        content: {'application/json': {schema: getModelSchemaRef(Movie)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Movie, {
            title: 'NewMovie',
          }),
        },
      },
    })
    movie: Movie,
  ): Promise<Movie> {
    return this.movieRepository.create(normalizeMovie(movie));
  }

  @get('/movies/count', {
    responses: {
      '200': {
        description: 'Movie model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(@param.where(Movie) where?: Where<Movie>): Promise<Count> {
    return this.movieRepository.count(where);
  }

  @get('/movies', {
    responses: {
      '200': {
        description: 'Array of Movie model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Movie, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(@param.filter(Movie) filter?: Filter<Movie>): Promise<Movie[]> {
    return this.movieRepository.find(filter);
  }

  @get('/movies/{id}', {
    responses: {
      '200': {
        description: 'Movie model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Movie, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: number,
    @param.filter(Movie, {exclude: 'where'})
    filter?: FilterExcludingWhere<Movie>,
  ): Promise<Movie> {
    return this.movieRepository.findById(id, filter);
  }

  @patch('/movies/{id}', {
    responses: {
      '204': {
        description: 'Movie PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Movie, {partial: true}),
        },
      },
    })
    movie: Movie,
  ): Promise<void> {
    await this.movieRepository.updateById(id, normalizeMovie(movie));
  }

  @del('/movies/{id}', {
    responses: {
      '204': {
        description: 'Movie DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: number): Promise<void> {
    await this.movieRepository.deleteById(id);
  }
}
