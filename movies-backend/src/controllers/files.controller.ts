import {inject} from '@loopback/core';
import {IsolationLevel, repository} from '@loopback/repository';
import {
  post,
  Request,
  requestBody,
  getModelSchemaRef,
  Response,
  RestBindings,
} from '@loopback/rest';
import multer from 'multer';
import fileToMovie from '../utils/fileToMovie';
import filterMoviesByTitle from '../utils/filterMoviesByTitle';
import {MovieTransactionalRepository, MovieRepository} from '../repositories';
import {Movie} from '../models';

export class FileUploadController {
  constructor(
    @repository(MovieTransactionalRepository)
    public movieTransactionalRepository: MovieTransactionalRepository,
    @repository(MovieRepository)
    public movieRepository: MovieRepository,
  ) {}

  /*
   * ACLARACIÓN: Este endpoint lo creé para experimentar con repos transaccionales y porque
   * me parece interesante interpretar el requerimieinto de manera transacional ya que es mas prolijo, seguro y consistente.
   * Sin embargo, para crear filtrando por titulo normalmente está implementado el /files.
   * En este caso puntual se busca a drede no filtrar en la creación los elementos duplicados
   * pese a que sería ideal hacerlo para cumplir el requerimiento solicitado. Pero
   * para que falle y la transaccionalidad haga lo suyo, al enviar un archivo con peliculas duplicadas
   * no creará ninguna.
   */
  @post('/transactional/files', {
    responses: {
      200: {
        description:
          'This endpoint import a batch of movies from a .csv. If it fails, no movie is created',
        'application/json': {
          schema: getModelSchemaRef(Movie, {
            title: 'NewMovie',
          }),
        },
      },
    },
  })
  async import(
    @requestBody.file()
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Movie[]> {
    const storage = multer.memoryStorage();
    const upload = multer({storage});

    const files = new Promise<object>((resolve, reject) => {
      this.getMovieFromRequest(upload, request, response, reject, resolve);
    });
    return files.then(async (data: any) => {
      const transaction = await this.movieTransactionalRepository.beginTransaction(
        IsolationLevel.READ_COMMITTED,
      );
      // Para esta parte del código prefiero el estilo "sincrónico" que propone await para mas claridad.
      try {
        const result = await this.movieTransactionalRepository.createAll(
          fileToMovie(data),
          {transaction},
        );
        await transaction.commit();
        return result;
      } catch (exception) {
        await transaction.rollback();
        throw exception;
      }
    });
  }

  /*
   *  Este endpoint es para cubrir el caso de negocio mencionado el enunciado de crear las peliculas que se puedan
   *  porque cumplen con las reglas de negocio. Se devuelven las peliculas creadas para comodidad del
   *  usuario de la API.
   */
  @post('/files', {
    responses: {
      200: {
        description:
          'This endpoint import a batch of movies from a .csv. If it fails, no movie is created',
        'application/json': {
          schema: getModelSchemaRef(Movie, {
            title: 'NewMovie',
          }),
        },
      },
    },
  })
  async importFiltered(
    @requestBody.file()
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<Movie[]> {
    const storage = multer.memoryStorage();
    const upload = multer({storage});

    const files = new Promise<object>((resolve, reject) => {
      this.getMovieFromRequest(upload, request, response, reject, resolve);
    });

    return files.then(async (fetchedFiles: any) => {
      const filteredMovies = await filterMoviesByTitle(
        fileToMovie(fetchedFiles),
        this.movieRepository,
      );
      return this.movieRepository.createAll(filteredMovies);
    });
  }

  private getMovieFromRequest(
    upload: multer.Multer,
    request: Request,
    response: Response<any>,
    reject: (reason?: any) => void,
    resolve: (value?: object | PromiseLike<object> | undefined) => void,
  ) {
    upload.any()(request, response, (err: unknown) => {
      if (err) reject('Error parsing the file');
      else {
        resolve(request.files);
      }
    });
  }
}
