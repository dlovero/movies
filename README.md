# Movies setup!
Bienvenido! Este es un paso a paso sobre como correr el proyecto de Movies.
___
___
## Pre requisitos
Para correr este proyecto es necesario tener dos cosas:
#### Una base de datos MySql (Versión 5.7 basta)
Para ello, si todavía no tenes MySql en tu computadora podes instalarla con el package manager de `Ubuntu`:
```
sudo apt update
sudo apt install mysql-server-5.7
```
Para `MacOs` mismo procedimiento con `brew`

Opcional: luego de instalarlo podes correr `sudo msyql_secure_installation` para setupear varios defaults, como la password para el usuario root.

**Si NO tiene pass tu usuario root**, Logueate a la consola con
```
sudo mysql -uroot
```
**Si tiene pass tu usuario root**, Logueate a la consola con
```
sudo mysql -u root -p
```

Una vez dentro, crea una base de datos llamada `moviesdb`. (Si todo sale bien deberias verla con `SHOW databases;`)
```
CREATE DATABASE moviesdb;
```

Ahora creá un usuario para la DB. El que viene configurado por default para el proyecto espera el username: `user` y la password: `pass`.
```
CREATE USER 'user'@'localhost' IDENTIFIED BY 'pass';
FLUSH PRIVILEGES;
```

Y le damos permisos en la DB que recién creamos. (A fines prácticos, todos los permisos)
```
GRANT ALL PRIVILEGES ON moviesdb.* TO 'user'@'localhost';
FLUSH PRIVILEGES;
```

_(Aclaración: Una vez instalado si aún no levantó el servicio (podes verlo con `sudo service mysql status`) podes
iniciarlo con `sudo service mysql start`)_

#### Node > 10.18.0 (o superior)

Para esto te recomiendo utilizar un version manager como `nvm`, podes ver mas info acá -> https://github.com/nvm-sh/nvm

Con `nvm` simplemente insntalas la versión que quieras, en mi caso la 10.18.0
```
nvm install 10.18.0
```
y cambias a la versión instalada que quieras con:
```
nvm use 10.18.0
```

#### Yarn como package manager

Particularmente prefiero yarn a npm como package manager, para instalarlo seguí esta guía:
https://classic.yarnpkg.com/en/docs/install/#debian-stable
___
___
## Corriendo la app!
Estos son los pasos para correr la app:
1. Clonar el repo (https://gitlab.com/dlovero/movies)
    `git clone git@gitlab.com:dlovero/movies.git && cd movies`
2. Instalar las dependencias
    `yarn install`
3. Ejecutar las migraciones
    `yarn migrate`
4. Correr las dos apps con un solo comando! (Por default: Backend puerto `3000`, frontend puerto `3001`)
    `yarn start`
(Aclaración, todo esto se puede hacer desde el root directory `/movies`, pero las migraciones hacen un cwd a ./movies-backend ara correrlas y el install también hace un cwd a ./movies-frontend y ./movies-backend para instalarlo)
___
**Los proyectos se pueden correr individualmente, por ejemplo:**

### Corriendo solo el backend:
1. Pararse en el subproject
    `cd movies-backend`
2. Instalar las dependencias del subproject
    `yarn install`
3. Correr las migraciones (opcional)
    `yarn migrate`
4. Levantar el backend
    `yarn start`

### Corriendo solo el frontend:
1. Pararse en el subproject
    `cd movies-backend`
2. Instalar las dependencias del subproject
    `yarn install`
3. Levantar el frontend
    `yarn start`

### Corriendo los tests:
1. Pararse en el subproject
    `cd movies-backend`
2. Correr los tests
    `yarn test`

___
___
### A mejorar:
- [ ] Mascarar el ID para no exponer el autogenerado de la DB directamente
- [ ] Tipar correctamente los any (Por ejemplo de `fileToMovie.ts`)
- [ ] Mejor manejo de como se convierte a string el buffer del archivo .csv enviado (`filesToMovie.ts`)
- [ ] Crear una interfaz reemplazable para el resultado del parseo de CSV
- [ ] Enmascarar fields al momento de hacer queries (`cataloj.js`)
- [ ] Fallbackear el search al backend si no existe el resultado dentro de los datos paginados (`catalog.js`)
- [ ] Agregar tests de controller a `files.controller.ts`
- [ ] Eventualmente agregar filtrado al endpoint transaccional de importar archivos (`files.controller.ts`)
- [ ] Tests automaticos mas extensivos