import React from "react";
import MaterialTable from "material-table";
import axios from "axios";
import swal from "sweetalert";

/*
 * ACLARACIÓN: Opte por utilizar MaterialTable por dos motivos:
 * 1) Está recomendado en la documentación oficial de Material UI (https://material-ui.com/components/tables/)
 * 2) Me pareció mas oportuno no gastar tiempo en el diseño del componente per se si no en intentar
 * aprender buenas prácticas de un framework nuevo para mi (Loopback) antes que el diseño del componente.
 * -----
 * La idea fue aprovechar la paginación para solamente traer resultados de queries que se correspondan a
 * la página seleccionada. El Search en principio solo funciona para los resultados paginados pero
 * idealmente si no hay resultados, lo mejor sería hacer un get por título al Back end. Por el momento
 * se me va de scope esa implementación.
 */

function Catalog() {
  const url = "http://localhost:3000/movies";
  const config = {
    headers: {
      "content-type": "application/json",
    },
  };
  const DEFAULT_PAGE_SIZE = 5;

  const [page, setPage] = React.useState(0);

  const [totalCount, setTotalCount] = React.useState(DEFAULT_PAGE_SIZE);

  const [state, setState] = React.useState({
    columns: [
      { title: "Titulo", field: "title" },
      { title: "Descripcion", field: "description" },
      { title: "Año lanzamiento", field: "year", type: "numeric" },
    ],
  });

  const deleteMovie = (url, oldData, config) => {
    axios
      .delete(`${url}/${oldData.id}`, config)
      .then(() => {
        setState((prevState) => {
          const data = [...prevState.data];
          data.splice(data.indexOf(oldData), 1);
          return { ...prevState, data };
        });
        fetchAndSetTotalCount();
      })
      .catch(() =>
        swal("Oh no =(", "Algo falló al borrar esta película", "error")
      );
  };

  const updateMovie = (url, oldData, newData, config) => {
    axios
      .patch(`${url}/${oldData.id}`, newData, config)
      .then(() => {
        setState((prevState) => {
          const data = [...prevState.data];
          data[data.indexOf(oldData)] = newData;
          return { ...prevState, data };
        });
      })
      .catch(() =>
        swal("Oh no =(", "Algo falló al editar esta película", "error")
      );
  };

  const createMovie = (url, newData, config) => {
    return axios
      .post(url, newData, config)
      .then(() => {
        setState((prevState) => {
          const data = [...prevState.data];
          data.push(newData);
          return { ...prevState, data };
        });
        fetchAndSetTotalCount();
      })
      .catch(() =>
        swal("Oh no =(", "Algo falló al crear esta película", "error")
      );
  };

  // TODO: We can mask fields asked here adding "fields":{...fields} to the URL.
  const urlPaginated = (page, pageSize) =>
    `${url}?filter={"offset":${page * pageSize},"limit":${pageSize}}`;

  const fetchAndSetData = (page, pageSize) => {
    axios.get(urlPaginated(page, pageSize), config).then((info) => {
      setState((prevState) => {
        return { ...prevState, data: info.data };
      });
    });
  };

  const fetchAndSetTotalCount = () => {
    axios.get(`${url}/count`, config).then((info) => {
      setTotalCount(info.data.count);
    });
  };

  const firstLoad = () => !state.data;

  const initializeData = () => {
    if (firstLoad()) {
      fetchAndSetData(page, DEFAULT_PAGE_SIZE);
      fetchAndSetTotalCount();
    }
  };

  initializeData();

  return (
    <MaterialTable
      title="Catalogo de peliculas"
      columns={state.columns}
      data={state.data}
      editable={{
        onRowAdd: (newData) =>
          new Promise((resolve) => {
            resolve();
            if (newData) {
              createMovie(url, newData, config);
            }
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            resolve();
            if (oldData) {
              updateMovie(url, oldData, newData, config);
            }
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            resolve();
            if (oldData) {
              deleteMovie(url, oldData, config);
            }
          }),
      }}
      onChangePage={(page, pageSize) => {
        setPage(page);
        fetchAndSetData(page, pageSize);
      }}
      onChangeRowsPerPage={(pageSize) => {
        fetchAndSetData(page, pageSize);
        fetchAndSetTotalCount();
      }}
      totalCount={totalCount}
      page={page}
    />
  );
}

export default Catalog;
