import React, { useState } from "react";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { FormHelperText } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { post } from "axios";
import swal from "sweetalert";

function Import() {
  const [file, setFile] = useState();

  const fileUpload = (file) => {
    const url = "http://localhost:3000/files";
    const formData = new FormData();
    formData.append("file", file.file);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    return post(url, formData, config);
  };

  const isValidUpload = () =>
    file && file.file.name && validExtension(file.file.name);

  const isValidInput = () =>
    !file || (file.file.name && validExtension(file.file.name));

  const validExtension = (fileName) =>
    fileName.split(".").pop().toLowerCase() === "csv";

  const doUpload = (e) => {
    fileUpload(file)
      .then((response) => {
        swal(
          "Éxito!",
          `Tus peliculas fueron importadas correctamente.
          Se crearon ${response.data.length} peliculas nuevas.`,
          "success"
        );
      })
      .catch((error) => {
        swal("Oh oh!", "Algo falló al importar tus películas", "error");
      });
  };

  const onChange = (e) => {
    setFile({ file: e.target.files[0] });
  };

  return (
    <React.Fragment>
      <Typography>Importá tus pelis con un .csv!</Typography>
      <FormControl>
        <Box m={1}>
          <Input type="file" onChange={onChange} />
          {!isValidInput() && (
            <FormHelperText error={true}>
              Formato inválido de archivo
            </FormHelperText>
          )}
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={doUpload}
          disabled={!isValidUpload()}
        >
          Importar
        </Button>
      </FormControl>
    </React.Fragment>
  );
}

export default Import;
