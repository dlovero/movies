import React from "react";
import Import from "./components/import";
import Catalog from "./components/catalog";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TabPanel from "./components/utils/tabpanel";
import AppBar from "@material-ui/core/AppBar";

function App() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  
  return (
    <React.Fragment>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab label="Películas" {...a11yProps(0)} />
          <Tab label="Importar" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Catalog />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Import />
      </TabPanel>
    </React.Fragment>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default App;
